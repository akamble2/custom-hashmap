
public class CustomData {

	String dataType;

	Boolean value = false;

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "CustomData [dataType=" + dataType + ", value=" + value + "]";
	}

}
