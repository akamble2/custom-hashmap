import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class Hashmap<K, V> {

	int Size;
	LinkedList[] Bucket;
	K k;
	V v;

	public Hashmap(int size) {

		Bucket = new LinkedList[size];
	}

	public Hashmap() {

		Bucket = new LinkedList[16];
	}

	public Object put(K k, V v) {
		if (k == null) {
			if (Bucket[0] == null) {
				Bucket[0] = new LinkedList();
			}
			if (Bucket[0].size() != 0) {
				Bucket[0].clear();
			}
			Bucket[0].add(new Entry(k, v));
		}

		else {
			int hash = hashing(k, Bucket.length);

			if (Bucket[hash] == null) {
				Bucket[hash] = new LinkedList();
			}
			int Counter = 0;
			int i1 = 0;

			for (; i1 < Bucket[hash].size(); i1++) {
				Entry entry = (Entry) Bucket[hash].get(i1);
				if (entry.k.equals(k)) {
					Counter = i1;
					break;
				}

			}

			if (i1 == Bucket[hash].size()) {
				Bucket[hash].add(new Entry(k, v));

			}

			else {
				Bucket[hash].remove(Counter);
				Bucket[hash].add(Counter, new Entry(k, v));
			}

		}

		return v;
	}

	private int hashing(K k2, int length) {

		char[] tempk2 = k2.toString().toCharArray();

		int hash = 0;

		for (int k2counter = 0; k2counter < tempk2.length; k2counter++) {

			hash = (hash + tempk2[k2counter] * k2counter);
		}

		return hash % length;
	}

	public void Display() {

		for (int i = 0; i < Bucket.length; i++) {
			if (Bucket[i] != null) {
				for (int j = 0; j < Bucket[i].size(); j++) {
					Entry e = (Entry) Bucket[i].get(j);

					System.out.println("Key" + e.k + "Value" + e.v);
				}
			}
		}

	}

	public V get(K k) {
		Entry e = null;
		int hash = 0;

		if (k != null) {
			hash = hashing(k, Bucket.length);
		}

		if (Bucket[hash] == null) {
			return null;
		}

		int j = 0;
		for (; j < Bucket[hash].size(); j++) {
			e = (Entry) Bucket[hash].get(j);
			if (e.k == null || e.k.equals(k)) {
				break;
			}

		}

		if (j == Bucket[hash].size()) {
			return null;
		}

		else {
			return (V) e.v;
		}

	}

	public void clear() {
		for (int Bucketindex = 0; Bucketindex < Bucket.length; Bucketindex++) {
			Bucket[Bucketindex] = null;
		}
	}

	public boolean isEmpty() {
		int Bucketindex = 0;

		for (; Bucketindex < Bucket.length; Bucketindex++) {
			if (Bucket[Bucketindex] != null) {
				break;

			}
		}

		if (Bucketindex == Bucket.length) {
			return true;
		} else {
			return false;
		}

	}

	public Set<K> keySet() {
		int Bucketindex = 0;
		Set s = new HashSet();

		for (; Bucketindex < Bucket.length; Bucketindex++) {
			if (Bucket[Bucketindex] != null) {
				for (int j = 0; j < Bucket[Bucketindex].size(); j++) {
					Entry e = (Entry) Bucket[Bucketindex].get(j);

					s.add(e.k);
				}

			}
		}

		return s;

	}

	@Override
	public String toString() {
		String data = "";
		for (int i = 0; i < Bucket.length; i++) {
			if (Bucket[i] != null) {
				for (int j = 0; j < Bucket[i].size(); j++) {
					Entry e = (Entry) Bucket[i].get(j);

					data = data.concat("Hashmap [k=" + e.k + ", v=" + e.v + "]");

				}
			}
		}
		return data;
	}

	/*
	 * public V remove(K key) { if(key==null) {
	 * 
	 * }
	 * 
	 * }
	 */

}
