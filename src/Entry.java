
public class Entry<K, V> {
	K k;
	V v;

	public Entry(K k, V v) {

		this.k = k;
		this.v = v;
	}

	@Override
	public String toString() {
		return "Entry [k=" + k + ", v=" + v + "]";
	}

}
